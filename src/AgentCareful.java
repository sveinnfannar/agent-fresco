
import weka.classifiers.Classifier;
import weka.core.Instances;

import java.util.ArrayList;

/**
 *
 *  Example agent:
 *
 * This agent always tries to run away from the other agent (chicken, chicken, ...).
 * If it cannot do so, it rests to gain stamina.
 *
 * @author      Yngvi Bjornsson
 *
 * @version     %I%, %G%
 *
 */
public class AgentCareful extends Agent {

    private int m_noThisAgent;     // Index of our agent (0 or 1).
    private int m_noOpponentAgent; // Inex of opponent's agent.

    public AgentCareful( CardDeck deck, int msConstruct, int msPerMove, int msLearn ) {
        super(deck, msConstruct, msPerMove, msLearn );
    }

    public void startGame(int noThisAgent, StateBattle stateBattle) {
        // Remember the indicies of the agents in the StateBattle.
        m_noThisAgent = noThisAgent;
        m_noOpponentAgent  = (noThisAgent == 0 ) ? 1 : 0; // can assume only 2 agents battling.
    }

    public void endGame(StateBattle stateBattle, double[] results) {
        // Nothing to do.
    }

    public Card act(StateBattle stateBattle ) {

        Card [] move = new Card[2];

        move[m_noOpponentAgent] = new CardRest();   // We assume the opponent just stays where he/she is,
                                                    // and then take the move that brings us as far away as possible.

        int oppStamina = stateBattle.getAgentState(m_noOpponentAgent).getStaminaPoints();
        
        Card bestCard = new CardRest();
        int minDistance = calcDistanceBetweenAgents( stateBattle );
        int oldMinDistance = minDistance;
        StateAgent asThis = stateBattle.getAgentState( m_noThisAgent );
        StateAgent asOpp  = stateBattle.getAgentState( m_noOpponentAgent );
        
        ArrayList<Card> allCards = m_deck.getCards();
        boolean inRange = false;
        for (Card card : allCards) {
        	if (card.getType() == Card.CardActionType.ctAttack && card.inAttackRange(asThis.getCol(), asThis.getRow(),
                    asOpp.getCol(), asOpp.getRow()))
        		inRange = true;
        }
        
        if (!inRange) {
        	return new CardRest();
        }
        
        else if (asThis.getHealthPoints() >= asOpp.getHealthPoints()) {
        	if (asThis.getStaminaPoints() > asOpp.getStaminaPoints() + 1) {
        		ArrayList<Card> cards = m_deck.getCards(asThis.getStaminaPoints());
        		for (Card card : cards ) {
        			if ( (card.getType() == Card.CardActionType.ctAttack) &&
        					card.inAttackRange( asThis.getCol(), asThis.getRow(),
                                            asOpp.getCol(), asOpp.getRow() ) ) {
        				return card;  // attack!
        			}
        		}
        	}
        }

        if (oppStamina <= 1) {
        	ArrayList<Card> cards = m_deck.getCards(asThis.getStaminaPoints());
        	for (Card card : cards ) {
                if ( (card.getType() == Card.CardActionType.ctAttack) &&
                        card.inAttackRange( asThis.getCol(), asThis.getRow(),
                                            asOpp.getCol(), asOpp.getRow() ) ) {
                      return card;  // attack!
                  }
        	}
        }
        
        ArrayList<Card> cards = m_deck.getCards( asThis.getStaminaPoints() );
        for ( Card card : cards ) {
            StateBattle bs = (StateBattle) stateBattle.clone();   // close the state, as play( ) modifies it.
            move[m_noThisAgent] = card;
            bs.play( move );
            int  distance = calcDistanceBetweenAgents( bs );
            if ( distance > minDistance ) {
                bestCard = card;
                minDistance = distance;
            }
            else if (card.getType() == Card.CardActionType.ctDefend && minDistance == oldMinDistance) {
            	bestCard = card;
            }
        }
        
        

        return bestCard;
    }

    public Classifier learn( Instances instances ) {
        // no learning
        return null;
    }

    private int calcDistanceBetweenAgents( StateBattle bs ) {

        StateAgent asFirst = bs.getAgentState( 0 );
        StateAgent asSecond = bs.getAgentState( 1 );

        return Math.abs( asFirst.getCol() - asSecond.getCol() ) + Math.abs( asFirst.getRow() - asSecond.getRow() );
    }

	//@Override
	//public void nextCard(Card nextCard)
	//{
		// TODO Auto-generated method stub
		
	//}

}
