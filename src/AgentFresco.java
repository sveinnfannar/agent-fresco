import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Random;

import weka.classifiers.Classifier;
import weka.classifiers.Evaluation;
import weka.classifiers.trees.J48;
import weka.core.Attribute;
import weka.core.FastVector;
import weka.core.Instance;
import weka.core.Instances;


public class AgentFresco extends Agent {
	
	private int m_noThisAgent;
	private int m_noOpponentAgent;
	private J48 m_classifier;
	private Instances m_trainingData;
	private Instances m_realData;
	private StateBattle m_previousStateBattle;
	private int m_recursiveDepth = 2; // How many games we check into the future.
	private int m_carloGames = 50; // How many monte carlo games we play for each leave. Lower if too many warnings.
	private ArrayList<Card> m_allCards;
	private int m_cardSize;
	private int m_noMovesToCheck = 3; // How many moves we expand out from on each state.
	
	public AgentFresco( CardDeck deck, int msConstruct, int msPerMove, int msLearn ) {
        super(deck, msConstruct, msPerMove, msLearn );
        
		m_classifier = new J48();
		m_allCards = m_deck.getCards();
		m_cardSize = m_allCards.size();
		m_realData = AgentFresco.createInstances(m_deck);
    }
	
	public void startGame(int noThisAgent, StateBattle stateBattle) {
        m_noThisAgent = noThisAgent;
        m_noOpponentAgent  = (noThisAgent == 0 ) ? 1 : 0;
	}
	
	public int actRecursive(StateBattle sb, int lvl)
	{
		if (lvl == 1)
		{
			return evaluateLeCarlo(sb);
		}
		Card opponentCard = this.getNextOpponentCard(sb);
		int[] bestValues = new int[m_noMovesToCheck];
		for(int i = 0; i < bestValues.length; i++)
			bestValues[i] = -1000; // TODO: Plz remove this magic constant..
		Card[] bestCards = new Card[m_noMovesToCheck];
		for(int i = 0; i < bestCards.length; i++)
			bestCards[i] = new CardRest();
		int bestValue = -1000; // TODO: Plz do not remove this magic constant..
		ArrayList<Card> availableCards = m_deck.getCards(sb.getAgentState(m_noThisAgent).getStaminaPoints());
		Card[] cards = new Card[2];
		cards[m_noOpponentAgent] = opponentCard;
		for (Card card : availableCards)
		{
			StateBattle cloned = (StateBattle)sb.clone();
			cards[m_noThisAgent] = card;
			if (cloned.play(cards))
			{
				if (cloned.getAgentState(m_noThisAgent).getHealthPoints() == 0)
				{
					if (cloned.getAgentState(m_noOpponentAgent).getHealthPoints() == 0)
					{
						if (bestValues[Min(bestValues)] < 0)
						{
							bestCards[Min(bestValues)] = card;
							bestValues[Min(bestValues)] = 0;
							if (bestValue < 0)
								bestValue = 0;
						}
					}
					continue;
				}
				if (cloned.getAgentState(m_noOpponentAgent).getHealthPoints() == 0)
				{
					if (bestValues[Min(bestValues)] < 1000)
					{
						bestCards[Min(bestValues)] = card;
						bestValues[Min(bestValues)] = 1000;
						if (bestValue < 1000)
							bestValue = 1000;
					}
				}
				if (cloned.getStepNumber() == cloned.getNumSteps())
				{
					if (bestValues[Min(bestValues)] < 0)
					{
						bestCards[Min(bestValues)] = card;
						bestValues[Min(bestValues)] = 0;
						if (bestValue < 0)
							bestValue = 0;
					}
				}
			}
			else
			{
				int prosperity = evaluate(cloned);
				if (prosperity > bestValues[Min(bestValues)])
				{
					bestCards[Min(bestValues)] = card;
					bestValues[Min(bestValues)] = prosperity;
				}
			}
		}
		for (int i = 0; i < bestValues.length; i++)
		{
			StateBattle cloned = (StateBattle)sb.clone();
			cards[m_noThisAgent] = bestCards[i];
			if (cloned.play(cards))
				continue;
			int prosperity = actRecursive(cloned, lvl - 1);
			if (prosperity > bestValue)
				bestValue = prosperity;
		}
		return bestValue;
	}

	public Card act(StateBattle stateBattle) {
		Card opponentCard = this.getNextOpponentCard(stateBattle);
		Card bestCard = new CardRest();
		int[] bestValues = new int[m_noMovesToCheck];
		for(int i = 0; i < bestValues.length; i++)
			bestValues[i] = -1000; // TODO: Plz do not remove these magic constants..
		Card[] bestCards = new Card[m_noMovesToCheck];
		for(int i = 0; i < bestCards.length; i++)
			bestCards[i] = new CardRest();
		int bestValue = -1000; // TODO: Plz remove this magic constant.. 
		ArrayList<Card> availableCards = m_deck.getCards(stateBattle.getAgentState(m_noThisAgent).getStaminaPoints());
		Card[] cards = new Card[2];
		cards[m_noOpponentAgent] = opponentCard;
		for (Card card : availableCards)
		{
			StateBattle cloned = (StateBattle)stateBattle.clone();
			cards[m_noThisAgent] = card;
			if (cloned.play(cards))
			{
				if (cloned.getAgentState(m_noThisAgent).getHealthPoints() == 0)
				{
					if (cloned.getAgentState(m_noOpponentAgent).getHealthPoints() == 0)
					{
						if (bestValues[Min(bestValues)] < 0)
						{
							bestCards[Min(bestValues)] = card;
							bestValues[Min(bestValues)] = 0;
							if (bestValue < 0)
							{
								bestValue = 0;
								bestCard = card;
							}
						}
					}
					continue;
				}
				if (cloned.getAgentState(m_noOpponentAgent).getHealthPoints() == 0)
				{
					if (bestValues[Min(bestValues)] < 1000)
					{
						bestCards[Min(bestValues)] = card;
						bestValues[Min(bestValues)] = 1000;
						if (bestValue < 1000)
						{
							bestValue = 1000;
							bestCard = card;
						}
					}
				}
				if (cloned.getStepNumber() == cloned.getNumSteps())
				{
					if (bestValues[Min(bestValues)] < 0)
					{
						bestCards[Min(bestValues)] = card;
						bestValues[Min(bestValues)] = 0;
						if (bestValue < 0)
						{
							bestValue = 0;
							bestCard = card;
						}
					}
				}
			}
			else
			{
				int prosperity = evaluate(cloned);
				if (prosperity > bestValues[Min(bestValues)])
				{
					bestCards[Min(bestValues)] = card;
					bestValues[Min(bestValues)] = prosperity;
				}
			}
		}
		for (int i = 0; i < bestValues.length; i++)
		{
			StateBattle cloned = (StateBattle)stateBattle.clone();
			cards[m_noThisAgent] = bestCards[i];
			if (cloned.play(cards))
				continue;
			int prosperity = actRecursive(cloned, m_recursiveDepth);
			if (prosperity > bestValue)
			{
				bestValue = prosperity;
				bestCard = bestCards[i];
			}
		}
		return bestCard;
	}
	
	private int evaluateLeCarlo(StateBattle sb)
	{
		int winnings = 0, tieWeight = 1, winWeight = 2;
		Card[] cards = new Card[2];
		for (int i = 0; i < m_carloGames; i++)
		{
			StateBattle cloned = (StateBattle)sb.clone();
			cards[m_noThisAgent] = m_allCards.get((int)Math.floor(Math.random() * m_cardSize));
			cards[m_noOpponentAgent] = m_allCards.get((int)Math.floor(Math.random() * m_cardSize));
			while(!cloned.play(cards))
			{
				cards[m_noThisAgent] = m_allCards.get((int)Math.floor(Math.random() * m_cardSize));
				cards[m_noOpponentAgent] = m_allCards.get((int)Math.floor(Math.random() * m_cardSize));
			}
			StateAgent us = cloned.getAgentState(m_noThisAgent);
			StateAgent opp = cloned.getAgentState(m_noOpponentAgent);
			if (us.getHealthPoints() == 0)
			{
				if (opp.getHealthPoints() == 0)
				{
					winnings += tieWeight;
				}
			}
			else if (opp.getHealthPoints() == 0)
			{
				winnings += winWeight;
			}
			else
			{
				winnings += tieWeight;
			}
		}
		return winnings;
	}
	
	private int calcDistanceBetweenAgents( StateBattle bs ) {

        StateAgent asFirst = bs.getAgentState( 0 );
        StateAgent asSecond = bs.getAgentState( 1 );

        return Math.abs( asFirst.getCol() - asSecond.getCol() ) + Math.abs( asFirst.getRow() - asSecond.getRow() );
    }
	
	private int evaluate (StateBattle sb)
	{
		int healthWeight = 10, // Higher number avoids losing health even further, negative numbers makes him a sadistic bastard.
			staminaWeight = 1, // Higher number avoids spending more stamina.
			distanceWeight = -2, // Higher prefers staying away from the other player, negative number prefers getting closer.
			changeWeight = 1, // Higher number makes a greater change when we fall behind or get ahead.
			cornerWeight = 5; // Higher number prefers getting enemy into corner, but avoids getting into corners yoruself.
		StateAgent us = sb.getAgentState(m_noThisAgent);
		StateAgent opp = sb.getAgentState(m_noOpponentAgent);
		int aHealth = us.getHealthPoints(),
			oHealth = opp.getHealthPoints(),
			aStamina = us.getHealthPoints(),
			oStamina = opp.getHealthPoints(),
			result = 0;
		switch(aHealth - oHealth)
		{
		case -2:
			distanceWeight = -2 + 2 * changeWeight;
			break;
		case -1:
			distanceWeight = -2 + changeWeight;
			break;
		case 0:
			distanceWeight = -2;
			break;
		case 1:
			distanceWeight = -2 - changeWeight;
			break;
		case 2:
			distanceWeight = -2 - 2 * changeWeight;
			break;
		default:
			break;
		}
		
		// Check health difference
		result += (aHealth - oHealth) * healthWeight;
		result += (aStamina - oStamina) * staminaWeight;
		if (aStamina == 0)
		{
			result -= CanHeHitMe(opp, us, sb) * healthWeight;
		}
		if (oStamina == 0)
		{
			result += CanHeHitMe(us, opp, sb) * healthWeight;
		}
		result += (calcDistanceBetweenAgents(sb)) * distanceWeight;
		if (isInCorner(us))
		{
			result -= cornerWeight;
		}
		if (isInCorner(opp))
		{
			result += cornerWeight;
		}
		return result;
	}
	
	private boolean isInCorner(StateAgent sa)
	{
		int col = sa.getCol();
		int row = sa.getRow();
		if((col == 0 && row == 0) || (col == 0 && row == 4) || (col == 4 && row == 4) || (col == 4 && row == 0))
			return true;
		return false;
	}
	
	private int CanHeHitMe(StateAgent attacker, StateAgent target, StateBattle sb)
	{
		int maxHP = 0;
		for(Card card : m_deck.getCards(attacker.getStaminaPoints()))
		{
			if (card.getType() == Card.CardActionType.ctAttack &&
				card.inAttackRange(attacker.getCol(), attacker.getRow(), target.getCol(), target.getRow()) &&
				card.getHitPoints() > maxHP)
			{
				maxHP = card.getHitPoints();
			}
		}
		return maxHP;
	}
	
	private int Min(int[] array)
	{
		int min = 0;
		for (int i = 1; i < array.length; i++)
		{
			if (array[i] < array[min])
				min = i;
		}
		return min;
	}
	
	public void endGame(StateBattle stateBattle, double[] results) {
		/*m_realData.setClassIndex(m_realData.numAttributes() - 1);
		
		// Evaluate the classifier
		try {
			Evaluation eval;
			eval = new Evaluation(m_trainingData);
			eval.crossValidateModel(m_classifier, m_trainingData, 10, new Random(1));
			//eval.evaluateModel(m_classifier, m_realData);
			//System.out.println("Correctly classified instances: " + (int)eval.pctCorrect() + "%");
			System.out.println(eval.toSummaryString("\nResults\n======\n", false));
		} catch (Exception e1) {
			e1.printStackTrace();
		}*/
	}
	
	private Card getNextOpponentCard(StateBattle stateBattle) {
		int cardIndex = 0;
		try {
			Instance instance = this.getInstanceForState(stateBattle, m_previousStateBattle);
			cardIndex = (int)m_classifier.classifyInstance(instance);
			if (stateBattle.getStepNumber() > 1)
			{
				//double value = m_realData.attribute(8).indexOfValue(m_previousStateBattle.getLastMoves()[m_noOpponentAgent].getName());
				//instance.setValue(8, value);
				m_realData.add(instance);
			}
			m_previousStateBattle = stateBattle;
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return m_deck.getCards().get(cardIndex);
	}
	
	private Instance getInstanceForState(StateBattle stateBattle) {
		return this.getInstanceForState(stateBattle, null);
	}
	
	private Instance getInstanceForState(StateBattle stateBattle, StateBattle previousState) {
		StateAgent myState = stateBattle.getAgentState(m_noThisAgent);
		StateAgent opponentState = stateBattle.getAgentState(m_noOpponentAgent);
		
		double values[] = new double[9]; // TODO: Remove this magic constant plz
		values[0] = opponentState.getCol();
        values[1] = opponentState.getRow();
        values[2] = opponentState.getHealthPoints();
        values[3] = opponentState.getStaminaPoints();
        values[4] = myState.getCol();
        values[5] = myState.getRow();
        values[6] = myState.getHealthPoints();
        values[7] = myState.getStaminaPoints();
        if (previousState != null && previousState.getLastMoves()[m_noOpponentAgent] != null)
        {
        	values[8] = m_realData.attribute(8).indexOfValue(previousState.getLastMoves()[m_noOpponentAgent].getName());
        }
        
        Instance instance = new Instance(1.0, values.clone());
		instance.setDataset(m_trainingData);
        return instance;
	}

	public Classifier learn(Instances instances) {
		// Preprocess the data
		m_trainingData = this.preprocessInstances(instances);
		
		// Learn
		try {
			m_classifier.buildClassifier(m_trainingData);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return m_classifier;
	}
	
	private Instances preprocessInstances(Instances instances)
	{
		Instances processed = AgentFresco.createInstances(m_deck);
		@SuppressWarnings("rawtypes")
		Enumeration instanceEnum = instances.enumerateInstances();
		while (instanceEnum.hasMoreElements())
		{
			Instance instance = (Instance) instanceEnum.nextElement();
			processed.add(this.preprocessInstance(instance));
		}
		
		return processed;
	}
	
	private Instance preprocessInstance(Instance instance)
	{
		return instance;
	}
	
    static private Instances createInstances( CardDeck deck )
    {
        FastVector attributes = new FastVector(); // Attributes
        FastVector actions = new FastVector();    // Class

        // Domain of class are all possible cards.
        for ( Card c : deck.getCards() ) {
            actions.addElement( c.getName() );
        }

        // Add the predicting attributes (all numeric)
        attributes.addElement(new Attribute("a_x"));
        attributes.addElement(new Attribute("a_y"));
        attributes.addElement(new Attribute("a_health"));
        attributes.addElement(new Attribute("a_stamina"));
        attributes.addElement(new Attribute("o_x"));
        attributes.addElement(new Attribute("o_y"));
        attributes.addElement(new Attribute("o_health"));
        attributes.addElement(new Attribute("o_stamina"));
        // Add the class, the action the a_ agent took in the given state (nominal).
        attributes.addElement(new Attribute("a_action", actions));
        
        Instances instances = new Instances( "AgentBattleHistory", attributes, 0 );
        instances.setClassIndex(instances.numAttributes() - 1);
        return instances;
    }
}
