import java.util.ArrayList;

import weka.classifiers.Classifier;
import weka.core.Instances;

/**
 *
 *  Example agent:
 *
 * This agent is simply a sitting duck.
 *
 * @author      Haflidi Orn Olafsson
 *
 * @version     %I%, %G%
 *
 */
public class AgentRandomDefense extends Agent {
	private int m_noThisDude;

    public AgentRandomDefense( CardDeck deck, int msConstruct, int msPerMove, int msLearn ) {
        super(deck, msConstruct, msPerMove, msLearn );
    }

    public void startGame(int noThisAgent, StateBattle stateBattle) {
        m_noThisDude = noThisAgent;
    }

    public void endGame( StateBattle stateBattle, double [] results ) {
        // No book-keeping needed.
    }

    public Card act( StateBattle stateBattle ) {
    	ArrayList<Card> cards = m_deck.getCards(stateBattle.getAgentState(m_noThisDude).getStaminaPoints());
    	ArrayList<Card> finalCards = new ArrayList<Card>();
    	finalCards.add(new CardRest());
    	for (Card card : cards)
    	{
    		if (card.getType() == Card.CardActionType.ctDefend)
    			finalCards.add(card);
    	}
    	return finalCards.get((int)Math.floor(Math.random() * finalCards.size()));
    }

    public Classifier learn( Instances instances  ) {
        // Too lazy to learn anything.
        return null;
    }
}
