import java.util.ArrayList;

import weka.classifiers.Classifier;
import weka.core.Instances;

/**
 *
 *  Example agent:
 *
 * This agent is simply a sitting duck.
 *
 * @author      Haflidi Orn Olafsson
 *
 * @version     %I%, %G%
 *
 */
public class AgentDefense extends Agent {
	private int m_noThisDude;

    public AgentDefense( CardDeck deck, int msConstruct, int msPerMove, int msLearn ) {
        super(deck, msConstruct, msPerMove, msLearn );
    }

    public void startGame(int noThisAgent, StateBattle stateBattle) {
        m_noThisDude = noThisAgent;
    }

    public void endGame( StateBattle stateBattle, double [] results ) {
        // No book-keeping needed.
    }

    public Card act( StateBattle stateBattle ) {
    	ArrayList<Card> cards = m_deck.getCards(stateBattle.getAgentState(m_noThisDude).getStaminaPoints());
    	Card bestCard = new CardRest();
    	for (Card card : cards)
    	{
    		if (card.getDefencePoints() > bestCard.getDefencePoints())
    			bestCard = card;
    	}
    	return bestCard;
    }

    public Classifier learn( Instances instances  ) {
        // Too lazy to learn anything.
        return null;
    }
}
