/**
 *
 *  This class implements a Card that attacks the square the agent is on as well as the next
 *  two squares (back and forth) horizontally.
 *
 * @author      Yngvi Bjornsson
 *
 * @version     %I%, %G%
 *
 */

public class CardUberAttack extends Card {

    private static Coordinate [] cardinalLongline = { coO, coE, coW, coE2, coW2, coN, coS, coSW, coNW, coNE, coSE };

    public CardUberAttack() {
        super( "cUberAttack", Card.CardActionType.ctAttack, 0, 0,-5, 3, 0, cardinalLongline );
    }
}
